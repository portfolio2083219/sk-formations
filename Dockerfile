FROM ubuntu:20.04

WORKDIR /app

VOLUME ["/app/skformations"]
COPY ./requirements.txt /app

RUN apt update
RUN apt -y install apt-utils
RUN apt -y install net-tools
RUN apt -y install python
RUN apt -y install python3-pip
RUN apt -y install mysql-client
RUN apt -y install libmysqlclient-dev
RUN apt -y upgrade
RUN pip install -r requirements.txt
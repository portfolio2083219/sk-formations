source setup-env.sh

docker run \
--name $SKF_APP_CONTAINER \
--env MYSQL_DATABASE=$SKF_DATABASE \
--env MYSQL_USER=$SKF_USER \
--env MYSQL_PASSWORD=$SKF_PASSWORD \
--env MYSQL_HOST=$SKF_DB_HOST \
--env APP_HOST=$SKF_APP_HOST \
--network host \
-tiv $PWD/skformations:/app/skformations \
$SKF_APP_IMAGE \
sh

source clean-containers.sh
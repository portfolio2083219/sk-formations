from django.shortcuts import render

def is_authenticated(request):
    if not request.user.is_authenticated:
        return render(request, 'login/login.html', {
            "message" : 'Vous n\'étes pas connecté!',
            }, status=401)
            
    return None

def is_admin(request):
    is_authenticated = is_authenticated(request)
    if is_authenticated is not None:
        return is_authenticated

    if not request.user.is_superuser:
        return render(request, 'error/error.html', {
            "title" : "Forbidden",
            "message" : 'Vous n\'avais pas le droit d\'accéder à cette page!',
            "status" : '403',
            }, status=403)

    return None
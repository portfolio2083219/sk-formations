from django.test import TestCase
from .models import *

class FormationsTestCase(TestCase):

    def setUp(self):
        filesroot = "skformations/static/files/"

        ### add type formation 
        t_qualif = TypeFormation(description="Qualifiante")
        t_certif = TypeFormation(description="Certifiante")
        t_dipl = TypeFormation(description="Diplômante")
        t_qualif.save()
        t_certif.save()
        t_dipl.save()

        ### branche
        filename = "branches.csv"
        with open(filesroot + filename, "r") as file:
            for line in file:
                b = BrancheFormation(description = line.strip())
                b.save()

        ### echeance
        filename = "echance.csv"
        sep = "§"
        file = open(filesroot + filename, "r")
        file.readline()
        for line in file:
            code = line.split(sep)[0]
            duree = int(line.split(sep)[1]) 
            e = Echeance(code_echeance = code, duree = duree)
            e.save()
        file.close()

        ### formations and attestations
        filename = "formations.csv"
        file = open(filesroot + filename, "r")
        file.readline()
        for line in file:
            intitule = line.split("§")[1]
            typeF = None
            if line.split("§")[1].endswith("ualifiant"):
                n = len(line.split("§")[1]) - len(" - Qualifiant")
                intitule = line.split("§")[1][:n]
                typeF = TypeFormation.objects.get(description="Qualifiante")
            elif line.split("§")[1].startswith("Titre"):
                typeF = TypeFormation.objects.get(description="Certifiante")
            else:
                typeF = TypeFormation.objects.get(description="Diplômante")

            stage = int(line.split("§")[5].split()[0]) if line.split("§")[5] != "N.O" else 0
            lien_cpf = line.split("§")[6] if line.split("§")[6] != "N.A" else None

            attestation = None
            try:
                attestation = AttestationFormation.objects.get(description=line.split("§")[4])
            except:
                attestation = AttestationFormation(description=line.split("§")[4])
                attestation.save()

            e = Formation(code_formation = int(line.split("§")[0]), intitule_formation = intitule, type_formation = typeF, duree_formation_mois = int(line.split("§")[2]), duree_formation_heures = int(line.split("§")[3]), attestation_formation = attestation, duree_stage_semaines = stage, lien_cpf=lien_cpf, tarif_sans_acompte = float(line.split("§")[7]) - 55, acompte = 55.0, lien_brochure = line.split("§")[9], branche_formation = BrancheFormation.objects.get(description = line.split("§")[10].strip()))

            e.save()

        file.close()

        ### echeances formation
        filename = "echeances_plain.csv"
        sep = "§"
        with open(filesroot + filename, "r") as file:
            file.readline()
            for line in file:
                f = Formation.objects.get(code_formation=int(line.split(sep)[0][:4]))
                f.echeances_tarif_plain.add(Echeance.objects.get(code_echeance=line.split(sep)[1][:-1]))
                f.save()

        filename = "echeances_15.csv"
        sep = "§"
        with open(filesroot + filename, "r") as file:
            file.readline()
            for line in file:
                f = Formation.objects.get(code_formation=int(line.split(sep)[0][:4]))
                f.echeances_tarif_minus15.add(Echeance.objects.get(code_echeance=line.split(sep)[1][:-1]))
                f.save()

        filename = "echeances_25.csv"
        sep = "§"
        with open(filesroot + filename, "r") as file:
            file.readline()
            for line in file:
                f = Formation.objects.get(code_formation=int(line.split(sep)[0][:4]))
                f.echeances_tarif_minus25.add(Echeance.objects.get(code_echeance=line.split(sep)[1][:-1]))
                f.save()


        ### insert others
        others = [
                {
                    "filename" : "prerequis_scolaire",
                    "class" : PrerequisScolaire,
                    "sep" : "§"
                    },{
                    "filename" : "prerequis_technique",
                    "class" : PrerequisTechnique,
                    "sep" : ","
                    },{
                    "filename" : "evolutions",
                    "class" : EvolutionFormation,
                    "sep" : "§"
                    },{
                    "filename" : "missions",
                    "class" : MissionFormation,
                    "sep" : "§"
                    },{
                    "filename" : "avantages",
                    "class" : AvantageFormation,
                    "sep" : "§"
                    },{
                    "filename" : "qualites_requis",
                    "class" : QualiteRequisFormation,
                    "sep" : "§"
                    },{
                    "filename" : "organisme_emploi",
                    "class" : OrganismeEmploiFormation,
                    "sep" : "§"
                    },
                ]
        for other in others:
            with open(filesroot + other["filename"] + '.csv') as file:
                file.readline()
                sep = other["sep"]
                current_model = other["class"]
                for line in file:
                    f = Formation.objects.get(code_formation = int(line.split(sep)[0]))
                    elm = current_model(formation = f, description = line.split(sep)[1])
                    elm.save()

    def test_formations(self):
        formation = Formation.objects.get(code_formation=4700)
        self.assertFalse(formation is None)
        self.assertEqual(len(formation.echeances_tarif_minus15.all()), 9)
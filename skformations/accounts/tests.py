from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

# Create your tests 

class AccountsTestCase(TestCase):

    def setUp(self):
        superuser = User.objects.create_superuser(username="superuser1", password="superuser1")
        user = User.objects.create_user(username="user1", password="user1")

    def test_permissions(self):
        superuser = User.objects.get(username="superuser1")
        user = User.objects.get(username="user1")
        self.assertTrue(superuser.is_superuser)
        self.assertFalse(user.is_superuser)

    def test_authentication(self):
        user1 = authenticate(username="superuser2", password="superuser2")
        self.assertTrue(user1 is None)
        user = authenticate(username="superuser1", password="superuser1")
        self.assertTrue(user is not None)

source setup-env.sh

source clean-containers.sh

docker run \
-dit \
--name $SKF_STATIC_SERVER_CONTAINER \
-p $SKF_STATIC_SERVER_PORT:80 \
-v $PWD/skformations/static/:/usr/local/apache2/htdocs/ \
httpd

docker run \
--name $SKF_APP_CONTAINER \
--env MYSQL_DATABASE=$SKF_DATABASE \
--env MYSQL_USER=$SKF_USER \
--env MYSQL_PASSWORD=$SKF_PASSWORD \
--env MYSQL_HOST=$SKF_DB_HOST \
--env APP_HOST=$SKF_APP_HOST \
--network host \
-tiv $PWD/skformations:/app/skformations \
$SKF_APP_IMAGE \
python3 skformations/manage.py test $1

source clean-containers.sh
source setup-env.sh

echo -n "Enter database root password : "
read -s mysql_root_pass
echo ""

select_created_users="SELECT User, Host from mysql.user where User = '$SKF_USER';"
created_users=`mysql -u root --password="$mysql_root_pass" -e "$select_created_users" | tr -s " " | tr "\t" " " | grep -v "User Host"`

i=0
user=""
host=""

for line in $created_users
do
    if [[ i -eq 0 ]] ; then user=$line ; fi
    if [[ i -eq 1 ]]
    then
        host=$line
        echo -n "Do you want to delete the user $user@'$host' (YES/NO)"
        read delete_user ;
        if [[ $delete_user = "YES" ]] 
        then
            mysql -u root --password="$mysql_root_pass" -e "drop user $user@'$host';"
            echo "$user@'$host' deleted."
        fi
    fi
    if [[ i -eq 0 ]]
    then
        i=1
    else
        i=0
    fi
done

create_user="CREATE USER '$SKF_USER'@'$SKF_APP_HOST' IDENTIFIED BY '$SKF_PASSWORD';"
grant_privilages="GRANT ALL PRIVILEGES ON $SKF_DATABASE.* TO '$SKF_USER'@'$SKF_APP_HOST'; GRANT ALL PRIVILEGES ON test_$SKF_DATABASE.* TO '$SKF_USER'@'$SKF_APP_HOST';"
mysql -u root --password="$mysql_root_pass" -e "$create_user"
mysql -u root --password="$mysql_root_pass" -e "$grant_privilages"

docker run \
--name $SKF_APP_CONTAINER \
--env MYSQL_DATABASE=$SKF_DATABASE \
--env MYSQL_USER=$SKF_USER \
--env MYSQL_PASSWORD=$SKF_PASSWORD \
--env MYSQL_HOST=$SKF_DB_HOST \
--env APP_HOST=$SKF_APP_HOST \
--network host \
-tiv $PWD/skformations:/app/skformations \
$SKF_APP_IMAGE \
python3 skformations/manage.py makemigrations

source clean-containers.sh

docker run \
--name $SKF_APP_CONTAINER \
--env MYSQL_DATABASE=$SKF_DATABASE \
--env MYSQL_USER=$SKF_USER \
--env MYSQL_PASSWORD=$SKF_PASSWORD \
--env MYSQL_HOST=$SKF_DB_HOST \
--env APP_HOST=$SKF_APP_HOST \
--network host \
-tiv $PWD/skformations:/app/skformations \
$SKF_APP_IMAGE \
python3 skformations/manage.py migrate

source clean-containers.sh

docker run \
--name $SKF_APP_CONTAINER \
--env MYSQL_DATABASE=$SKF_DATABASE \
--env MYSQL_USER=$SKF_USER \
--env MYSQL_PASSWORD=$SKF_PASSWORD \
--env MYSQL_HOST=$SKF_DB_HOST \
--env APP_HOST=$SKF_APP_HOST \
--network host \
-tiv $PWD/skformations:/app/skformations \
$SKF_APP_IMAGE \
python3 skformations/manage.py createsuperuser

source clean-containers.sh